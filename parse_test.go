package main

import "testing"

var parseTests = []string {
	"a",
	"(quote a)",
	"()",
	"(() ())",
	"(a . b)",
}


func TestParse(t *testing.T) {
	for i, input := range parseTests {
		p := parse(input)	
		if input != str(p) {
			t.Errorf("#%d: input:%v - parsed:%v - str:%v", i, input, p, str(p))
		}
	}
}
