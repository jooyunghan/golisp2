package main

import (
	V "container/vector"
	U "unicode"
)

func parse(s string) interface{} {
	return new(tokenizer).do(s)
}

type tokenizer struct {
	token string
	stack *V.Vector
}

func (t *tokenizer) do(s string) interface{} {
	t.token = ""
	t.stack = new(V.Vector)
	resultHolder := new(V.Vector)
	t.stack.Push(resultHolder) 
	for _, c := range s {
		if U.IsSpace(c) { 
			t.emit()
		} else if c == '(' {
			t.emit()
			t.beginList()
		} else if c == ')' {
			t.emit()
			t.endList()
		} else {
			t.token += string(c)
		}
	}
	t.emit()
	return resultHolder.Last()
}

func (t *tokenizer) emit() {
	if len(t.token)>0 {
		t.stack.Last().(*V.Vector).Push(t.token)
		t.token = ""
	}
}	

func (t *tokenizer) beginList() {
	t.stack.Push(new(V.Vector))
}

func (t *tokenizer) endList() {
	v := t.stack.Pop().(*V.Vector)
	top := t.stack.Last().(*V.Vector)
	top.Push(listV([]interface{}(*v)))
}
